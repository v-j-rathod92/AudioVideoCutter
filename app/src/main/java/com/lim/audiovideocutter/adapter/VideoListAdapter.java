package com.lim.audiovideocutter.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lim.audiovideocutter.R;
import com.lim.audiovideocutter.activity.VideoSelectActivity;
import com.lim.audiovideocutter.model.VideoData;
import com.lim.audiovideocutter.utils.ScreenUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.process.BitmapProcessor;

import java.util.List;

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.ViewHolder> {

    private Context mContext;
    private List<VideoData> videoDataList;

    public VideoListAdapter(Context mContext, List<VideoData> videoDataList) {
        this.mContext = mContext;
        this.videoDataList = videoDataList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_video_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        int imgHeight = ScreenUtils.getScreenWidth(mContext) / 2;
        holder.imgThumbNail.getLayoutParams().height = imgHeight;
        holder.imgThumbNail.requestLayout();

        DisplayImageOptions options = new DisplayImageOptions.Builder().showImageForEmptyUri((int) R.color.colorPrimary).cacheInMemory(true).resetViewBeforeLoading(true).cacheInMemory(true).cacheOnDisk(true).imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2).bitmapConfig(Bitmap.Config.ARGB_8888).delayBeforeLoading(100).displayer(new SimpleBitmapDisplayer()).postProcessor(new BitmapHelper()).displayer(new SimpleBitmapDisplayer()).build();
        ImageLoader.getInstance().displayImage((this.videoDataList.get(position)).VideoUri.toString(), holder.imgThumbNail, options);


        holder.txtTitle.setText(videoDataList.get(position).videoName);

        holder.imgThumbNail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((VideoSelectActivity) mContext).onVideoSelected(videoDataList.get(position).videoSDPath);
            }
        });
    }

    @Override
    public int getItemCount() {
        return videoDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgThumbNail;
        TextView txtTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            imgThumbNail = itemView.findViewById(R.id.imgThumbNail);
            txtTitle = itemView.findViewById(R.id.txtTitle);
        }
    }

    class BitmapHelper implements BitmapProcessor {
        BitmapHelper() {
        }

        public Bitmap process(Bitmap bmp) {
            return Bitmap.createScaledBitmap(bmp, 150, 150, false);
        }
    }
}
