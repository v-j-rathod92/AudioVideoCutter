package com.lim.audiovideocutter.adapter;

import android.content.ContentUris;
import android.content.Context;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lim.audiovideocutter.R;
import com.lim.audiovideocutter.activity.AudioListActivity;
import com.lim.audiovideocutter.model.AudioData;
import com.nostra13.universalimageloader.core.DisplayImageOptions.Builder;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.ArrayList;

/**
 * Created by rgi-40 on 20/3/18.
 */

public class AudioListAdapter extends RecyclerView.Adapter<AudioListAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<AudioData> videoDataList;
    private ImageLoader imageLoader;

    public AudioListAdapter(Context mContext, ArrayList<AudioData> videoDataList, ImageLoader imageLoader) {
        this.mContext = mContext;
        this.videoDataList = videoDataList;
        this.imageLoader = imageLoader;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layou_audio_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        if (position % 2 == 0) {
            holder.linearLayoutBackground.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorDivider1));
        } else {
            holder.linearLayoutBackground.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorDivider2));
        }
        Uri albumArtUri = ContentUris.withAppendedId(Uri.parse("content://media/external/audio/albumart"),
                videoDataList.get(position).getAlbumid().longValue());

        imageLoader.displayImage(albumArtUri.toString(), holder.imgThumbNail, new Builder().
                cacheOnDisk(true).cacheInMemory(true).resetViewBeforeLoading(true).
                imageScaleType(ImageScaleType.EXACTLY).displayer(new RoundedBitmapDisplayer(50)).
                showImageForEmptyUri(R.drawable.audio_file)
                .showImageOnFail(R.drawable.audio_file)
                .showImageOnLoading(R.drawable.audio_file).build());

        holder.txtTitle.setText(videoDataList.get(position).getAudioName());
        holder.txtDuration.setText(mContext.getString(R.string.duration) + " : " + videoDataList.get(position).getDuration());
        holder.txtSize.setText(mContext.getString(R.string.size) + " : " + videoDataList.get(position).getSize());

        holder.linearLayoutBackground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AudioListActivity) mContext).onAudioSelected(videoDataList.get(position).getAudioPath());
            }
        });
    }

    @Override
    public int getItemCount() {
        return videoDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linearLayoutBackground;
        ImageView imgThumbNail;
        TextView txtTitle, txtDuration, txtSize;

        public ViewHolder(View itemView) {
            super(itemView);

            linearLayoutBackground = itemView.findViewById(R.id.linearLayoutBackground);
            imgThumbNail = itemView.findViewById(R.id.imgThumbNail);
            txtTitle = itemView.findViewById(R.id.txtTitle);
            txtDuration = itemView.findViewById(R.id.txtDuration);
            txtSize = itemView.findViewById(R.id.txtSize);
        }
    }
}
