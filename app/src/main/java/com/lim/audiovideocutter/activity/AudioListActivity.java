package com.lim.audiovideocutter.activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.lim.audiovideocutter.R;
import com.lim.audiovideocutter.adapter.AudioListAdapter;
import com.lim.audiovideocutter.model.AudioData;
import com.lim.audiovideocutter.utils.ViewHelper;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import xyz.danoz.recyclerviewfastscroller.vertical.VerticalRecyclerViewFastScroller;

/**
 * Created by rgi-40 on 19/3/18.
 */

public class AudioListActivity extends AppCompatActivity {
    private Context mContext;
    private RecyclerView recyclerViewAudio;
    private ArrayList<AudioData> videoDataList;
    private AudioListAdapter adapter;
    private ProgressBar progressBar;
    private LinearLayout layoutEmpty;
    private ImageLoader imageLoader;
    private VerticalRecyclerViewFastScroller fastScroller;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_audio_list);

        init();
    }

    private void init() {
        mContext = this;
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.title_select_audio));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        videoDataList = new ArrayList<>();

        recyclerViewAudio = findViewById(R.id.recyclerViewAudio);
        recyclerViewAudio.setLayoutManager(new LinearLayoutManager(mContext));
        layoutEmpty = findViewById(R.id.layoutEmpty);
        progressBar = findViewById(R.id.progressBar);
        fastScroller = findViewById(R.id.fastScroller);

        initImageLoader();
        new LoadCursorData().execute();
    }

    private class LoadCursorData extends AsyncTask<Void, Void, Boolean> {
        Cursor cursor = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            String folderName = getResources().getString(R.string.audio_video_cutter_data);

            cursor = getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    new String[]{"_id", "_data", "_display_name", "_size", "duration", "date_added", "album", "album_id"},
                    "_data not like ? ",
                    new String[]{"%" + folderName + "%"},
                    "date_added DESC");

            if (cursor.moveToFirst()) {
                do {
                    String videoName = this.cursor.getString(this.cursor.getColumnIndexOrThrow("_display_name"));
                    String videoPath = this.cursor.getString(this.cursor.getColumnIndex("_data"));
                    long f_size = this.cursor.getLong(this.cursor.getColumnIndexOrThrow("duration"));

                    String converted = String.format("%02d:%02d", new Object[]{Long.valueOf(TimeUnit.MILLISECONDS.toMinutes(f_size)), Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(f_size) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(f_size)))});

                    int recordedVideoFileSize = this.cursor.getInt(this.cursor.getColumnIndexOrThrow("_size"));
                    Long albumId = Long.valueOf(this.cursor.getLong(this.cursor.getColumnIndexOrThrow("album_id")));

                    if (recordedVideoFileSize > 0) {
                        videoDataList.add(new AudioData(videoName, videoPath, converted, readableByteCount((long) recordedVideoFileSize, true), 0, false,
                                albumId));
                    }
                } while (cursor.moveToNext());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            if (videoDataList.size() > 0) {
                adapter = new AudioListAdapter(mContext, videoDataList, imageLoader);
                recyclerViewAudio.setAdapter(adapter);
                fastScroller.setVisibility(View.VISIBLE);
                fastScroller.setRecyclerView(recyclerViewAudio);
                recyclerViewAudio.addOnScrollListener(fastScroller.getOnScrollListener());
                ViewHelper.hideList(recyclerViewAudio, layoutEmpty, false);
            } else {
                ViewHelper.hideList(recyclerViewAudio, layoutEmpty, true);
            }
            progressBar.setVisibility(View.GONE);
        }
    }

    public static String readableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < ((long) unit)) {
            return bytes + " B";
        }
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(((int) (Math.log((double) bytes) / Math.log((double) unit))) - 1) + (si ? "" : "i");
        return String.format("%.1f %sB", new Object[]{Double.valueOf(((double) bytes) / Math.pow((double) unit, (double) ((int) (Math.log((double) bytes) / Math.log((double) unit))))), pre});
    }

    private void initImageLoader() {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext()).memoryCache(new WeakMemoryCache()).defaultDisplayImageOptions(new DisplayImageOptions.Builder().cacheInMemory().cacheOnDisc().bitmapConfig(Bitmap.Config.RGB_565).displayer(new FadeInBitmapDisplayer(400)).build()).build();
        this.imageLoader = ImageLoader.getInstance();
        this.imageLoader.init(config);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (this.imageLoader != null) {
            this.imageLoader.clearDiskCache();
            this.imageLoader.clearMemoryCache();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onAudioSelected(String audioPath) {
        Intent intent = new Intent(mContext, AudioCutActivity.class);
        intent.putExtra(AudioCutActivity.AUDIO_PATH, audioPath);
        startActivity(intent);
    }
}
