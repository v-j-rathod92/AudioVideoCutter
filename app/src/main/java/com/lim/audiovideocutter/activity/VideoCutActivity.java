package com.lim.audiovideocutter.activity;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.ads.AdView;
import com.lim.audiovideocutter.AdHandler;
import com.lim.audiovideocutter.R;

/**
 * Created by rgi-40 on 21/3/18.
 */

public class VideoCutActivity extends BaseActivity implements View.OnClickListener {
    public static final String VIDEO_URI = "video_uri";
    private Button btnCutVideo;
    private AdView adView;
    private AdHandler adHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_video_cut);

        init();
        setListeners();
        loadAds();
    }

    private void init() {
        mContext = this;

        videoView = findViewById(R.id.videoView);
        txtStart = findViewById(R.id.txtStart);
        txtEnd = findViewById(R.id.txtEnd);
        videoSliceSeekBar = findViewById(R.id.videoSliceSeekBar);
        playPauseButton = findViewById(R.id.playPauseButton);
        btnCutVideo = findViewById(R.id.btnCutVideo);
        adView = findViewById(R.id.adView);

        sourcePath = getIntent().getStringExtra(VIDEO_URI);

        if (sourcePath != null && !sourcePath.equals("")) {
            videoView.setVideoURI(Uri.parse(sourcePath));
        }

        videoView.seekTo(100);

        prepareVideoView();
        loadFFMpegBinary();
    }

    private void setListeners() {
        playPauseButton.setOnClickListener(this);
        btnCutVideo.setOnClickListener(this);
    }

    private void loadAds() {
        adHandler = AdHandler.getInstance();
        adHandler.loadBannerAd(adView, this);
        adHandler.showFullScrenReloadAd();
    }

    @Override
    protected void onResume() {
        super.onResume();
        videoView.seekTo(200);
    }
}
