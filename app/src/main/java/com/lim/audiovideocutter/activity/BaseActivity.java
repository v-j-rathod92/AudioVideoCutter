package com.lim.audiovideocutter.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.lim.audiovideocutter.MaterialPlayPauseButton;
import com.lim.audiovideocutter.R;
import com.lim.audiovideocutter.model.VideoPlayerState;
import com.lim.audiovideocutter.utils.TimeUtils;
import com.lim.audiovideocutter.view.VideoSliceSeekBar;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by rgi-40 on 23/3/18.
 */

public class BaseActivity extends AppCompatActivity implements View.OnClickListener {
    protected Context mContext;
    private static final String TAG = "COMMAND";
    private String timeRe = "\\btime=\\b\\d\\d:\\d\\d:\\d\\d.\\d\\d";
    private FFmpeg ffmpeg;
    private ProgressDialog dialog;
    protected VideoView videoView;
    protected VideoSliceSeekBar videoSliceSeekBar;
    protected MaterialPlayPauseButton playPauseButton;
    private double percen = 0.0d;
    private float toatalSecond;
    protected int last = 0, startVTime = 0, endVTime = 0;
    protected String sourcePath, endTime, destinationPath;
    protected StateObserver videoStateObserver = new StateObserver();
    protected TextView txtStart, txtEnd;
    private boolean playPause = false;
    protected VideoPlayerState videoPlayerState = new VideoPlayerState();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadFFMpegBinary();
    }


    protected void loadFFMpegBinary() {
        try {
            if (ffmpeg == null) {
                ffmpeg = FFmpeg.getInstance(this);
            }
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    showUnsupportedExceptionDialog();
                }

                @Override
                public void onSuccess() {
                    Log.d(TAG, "ffmpeg : correct Loaded");
                }
            });
        } catch (FFmpegNotSupportedException e) {
            showUnsupportedExceptionDialog();
        } catch (Exception e) {
            Log.d(TAG, "EXception no controlada : " + e);
        }
    }

    private void showUnsupportedExceptionDialog() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Not Supported")
                .setMessage("Device Not Supported")
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .create()
                .show();
    }

    protected void showProgress() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Cutting Audio... " + String.format("%.0f", new Object[]{Double.valueOf(percen)}) + "%");
        dialog.setCancelable(false);
        dialog.show();
    }


    protected void execFFmpegBinary(final String[] command, final int which) {
        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {
                    Log.d(TAG, "FAILED with output : " + s);
                    dialog.dismiss();
                }

                @Override
                public void onSuccess(String s) {
                    Log.d(TAG, "Success command : ffmpeg " + s);
                    dialog.dismiss();
                    showOpenFileDialog(which);
                }

                @Override
                public void onProgress(String s) {
                    Log.d(TAG, "Started command : ffmpeg " + s);
                    durationToprogtess(s);
                }

                @Override
                public void onStart() {
                    Log.d(TAG, "Started command : ffmpeg " + command);

                }

                @Override
                public void onFinish() {
                    Log.d(TAG, "Finished command : ffmpeg " + command);
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // do nothing for now
        }
    }


    private int durationToprogtess(String input) {
        int progress = 0;
        Matcher matcher = Pattern.compile(this.timeRe).matcher(input);
        int MINUTE = 1 * 60;
        int HOUR = MINUTE * 60;
        if (TextUtils.isEmpty(input) || !input.contains("time=")) {
//            Log.e("time", "not contain time " + input);
            return this.last;
        }
        while (matcher.find()) {
            String time = matcher.group();
            String[] splitTime = time.substring(time.lastIndexOf(61) + 1).split(":");
            float hour = ((Float.valueOf(splitTime[0]).floatValue() * ((float) HOUR)) + (Float.valueOf(splitTime[1]).floatValue() * ((float) MINUTE))) + Float.valueOf(splitTime[2]).floatValue();
            this.toatalSecond = (float) Integer.parseInt(String.valueOf(this.videoView.getDuration() / 1000));
            progress = (int) ((100.0f * hour) / this.toatalSecond);
            updateInMili(hour);
        }
        this.last = progress;
        return progress;
    }

    private void showOpenFileDialog(final int mediaType) {
        String title, message;

        if (mediaType == 1) {
            title = "Video Cut";
            message = "Your video has been stored in \"" + destinationPath + "\"";
        } else {
            title = "Audio Cut";
            message = "Your audio has been stored in \"" + destinationPath + "\"";
        }

        AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Open", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(destinationPath));
                        String type;
                        if (mediaType == 1) {
                            type = "video/mp4";
                        } else {
                            type = "audio/*";
                        }
                        intent.setDataAndType(Uri.parse(destinationPath), type);
                        startActivity(Intent.createChooser(intent, "Open Video"));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        dialog.show();

    }

    protected String getTimeForTrackFormat(int timeInMills, boolean display2DigitsInMinsSection) {
        int hour = timeInMills / TimeUtils.MilliSeconds.ONE_HOUR;
        int minutes = timeInMills / TimeUtils.MilliSeconds.ONE_MINUTE;
        int seconds = (timeInMills - ((minutes * 60) * 1000)) / 1000;
        String result = (!display2DigitsInMinsSection || hour >= 10) ? "" : "0";
        StringBuilder append = new StringBuilder().append(result + hour + ":");
        String str = (!display2DigitsInMinsSection || minutes >= 10) ? "" : "0";
        result = append.append(str).toString() + (minutes % 60) + ":";
        if (seconds < 10) {
            return result + "0" + seconds;
        }
        return result + seconds;
    }


    private void updateInMili(final float time) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                percen = (((double) time) * 100.0d) / ((double) toatalSecond);
                if (percen + 15.0d > 100.0d) {
                    percen = 100.0d;
                } else {
                    BaseActivity viewVideo = BaseActivity.this;
                    viewVideo.percen += 15.0d;
                }
                dialog.setMessage("Compressing video.. " + String.format("%.0f", new Object[]{Double.valueOf(percen)}) + "%");
            }
        });
    }


    class StateObserver extends Handler {
        private boolean alreadyStarted;
        private Runnable observerWork;

        class ObserverWorker implements Runnable {

            public void run() {
                startVideoProgressObserving();
            }
        }

        private StateObserver() {
            this.alreadyStarted = false;
            this.observerWork = new ObserverWorker();
        }

        protected void startVideoProgressObserving() {
            if (!this.alreadyStarted) {
                this.alreadyStarted = true;
                sendEmptyMessage(0);
            }
        }

        public void handleMessage(Message msg) {
            this.alreadyStarted = false;
            videoSliceSeekBar.videoPlayingProgress(videoView.getCurrentPosition());
            txtStart.setText(getTimeForTrackFormat(videoView.getCurrentPosition(), true));
            if (!videoView.isPlaying() || videoView.getCurrentPosition() >= videoSliceSeekBar.getRightProgress()) {
                if (videoView.isPlaying()) {
                    pausePlaying();
                }
                txtStart.setText(getTimeForTrackFormat(0, true));
                videoSliceSeekBar.setSliceBlocked(false);
                videoSliceSeekBar.removeVideoStatusThumb();
                return;
            }
            postDelayed(this.observerWork, 50);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (videoView != null && videoView.isPlaying()) {
            pausePlaying();
        }
    }

    protected void pausePlaying() {
        videoView.pause();
        playPauseButton.setToPlay();
        playPause = false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.playPauseButton:
                if (playPause) {
                    playPauseButton.setToPlay();
                    playPause = false;
                } else {
                    playPauseButton.setToPause();
                    playPause = true;
                }

                playPauseClick();
                break;

            case R.id.btnCutVideo:
                cutVideo();
                break;

            case R.id.btnCutAudio:
                cutAudio();
                break;
        }
    }

    private void playPauseClick() {
        if (videoView.isPlaying()) {
            videoView.pause();
            videoSliceSeekBar.setSliceBlocked(false);
            videoSliceSeekBar.removeVideoStatusThumb();
            return;
        }

        videoView.seekTo(videoSliceSeekBar.getLeftProgress());
        videoView.start();
        videoSliceSeekBar.videoPlayingProgress(videoSliceSeekBar.getLeftProgress());
        videoStateObserver.startVideoProgressObserving();
    }


    private void cutVideo() {
        if (videoView.isPlaying()) {
            pausePlaying();
        }

        prepareFileAndFolder(1);
        showProgress();

        String starTime = "" + (startVTime / 1000);
        String duration = "" + ((endVTime / 1000) - (startVTime / 1000));

        String[] args = new String[]{"-y", "-ss", starTime, "-i", sourcePath, "-t", duration, "-codec:v", "copy", "-codec:a", "copy", destinationPath};
        execFFmpegBinary(args, 1);
    }


    private void cutAudio() {
        if (videoView.isPlaying()) {
            pausePlaying();
        }

        prepareFileAndFolder(2);
        showProgress();

        String starTime = "" + (startVTime / 1000);
        String duration = "" + ((endVTime / 1000) - (startVTime / 1000));

        String[] args = new String[]{"-y", "-i", sourcePath, "-acodec", "copy", "-strict", "experimental", "-ss", starTime, "-t", duration, destinationPath};
        execFFmpegBinary(args, 2);
    }

    private void prepareFileAndFolder(int which) {
        String filename = sourcePath.substring(sourcePath.lastIndexOf("/") + 1);
        String audioName = filename.substring(0, filename.lastIndexOf("."));
        String extension = filename.substring(filename.lastIndexOf("."));

        String type;
        if (which == 1) {
            type = "/Video/";
        } else {
            type = "/Audio/";
        }

        File dest = new File(Environment.getExternalStorageDirectory() + "/" + getString(R.string.app_name) + type);
        if (!dest.exists()) {
            dest.mkdirs();
        }
        destinationPath = dest.getAbsolutePath() + "/" + audioName + extension;
    }


    protected void prepareVideoView() {
        videoView.setVideoPath(sourcePath);
        videoView.seekTo(200);
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Toast.makeText(mContext, getString(R.string.not_supported), Toast.LENGTH_LONG).show();
                return true;
            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                pausePlaying();
            }
        });

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                videoSliceSeekBar.setSeekBarChangeListener(new VideoSliceSeekBar.SeekBarChangeListener() {
                    @Override
                    public void SeekBarValueChanged(int leftThumb, int rightThumb) {
                        if (videoSliceSeekBar.getSelectedThumb() == 1) {
                            videoView.seekTo(videoSliceSeekBar.getLeftProgress());
                        }
                        txtStart.setText(getTimeForTrackFormat(leftThumb, true));
                        txtEnd.setText(getTimeForTrackFormat(rightThumb, true));
                        videoPlayerState.setStart(leftThumb);
                        endTime = getTimeForTrackFormat(rightThumb, true);
                        videoPlayerState.setStop(rightThumb);
                        startVTime = leftThumb;
                        endVTime = rightThumb;
                    }
                });

                endTime = getTimeForTrackFormat(mp.getDuration(), true);
                videoSliceSeekBar.setMaxValue(mp.getDuration());
                videoSliceSeekBar.setLeftProgress(0);
                videoSliceSeekBar.setRightProgress(mp.getDuration());
                videoSliceSeekBar.setProgressMinDiff(0);

            }
        });

        endTime = getTimeForTrackFormat(videoView.getDuration(), true);
    }
}
