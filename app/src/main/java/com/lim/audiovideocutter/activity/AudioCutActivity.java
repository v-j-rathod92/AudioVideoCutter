package com.lim.audiovideocutter.activity;

import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.gms.ads.AdView;
import com.lim.audiovideocutter.AdHandler;
import com.lim.audiovideocutter.R;
import com.squareup.picasso.Picasso;

/**
 * Created by rgi-40 on 21/3/18.
 */

public class AudioCutActivity extends BaseActivity implements View.OnClickListener {
    public static final String AUDIO_PATH = "audio_path;";
    private Button btnCutAudio;
    private ImageView imgThumbNail;
    private AdView adView;
    private AdHandler adHandler;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_audio_cut);
        init();
        setListeners();
        loadAds();
    }

    private void init() {
        mContext = this;

        videoView = findViewById(R.id.videoView);
        txtStart = findViewById(R.id.txtStart);
        txtEnd = findViewById(R.id.txtEnd);
        imgThumbNail = findViewById(R.id.imgThumbNail);
        videoSliceSeekBar = findViewById(R.id.videoSliceSeekBar);
        playPauseButton = findViewById(R.id.playPauseButton);
        btnCutAudio = findViewById(R.id.btnCutAudio);
        adView = findViewById(R.id.adView);

        sourcePath = getIntent().getStringExtra(AUDIO_PATH);

        try {
            setThumbNail(sourcePath);
        } catch (Exception e) {
            e.printStackTrace();
        }

        prepareVideoView();
        loadFFMpegBinary();
    }

    private void setListeners() {
        playPauseButton.setOnClickListener(this);
        btnCutAudio.setOnClickListener(this);
    }

    private void loadAds() {
        adHandler = AdHandler.getInstance();
        adHandler.loadBannerAd(adView, this);
        adHandler.showFullScrenReloadAd();
    }

    public void setThumbNail(String songPath) {
        Cursor cursor = getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, new String[]{"_id", "_data", "_display_name", "_size", "duration", "date_added", "album", "album_id"}, "_data  like ?", new String[]{"%" + songPath + "%"}, " _id DESC");
        if (cursor.moveToFirst()) {
            Long albumId = Long.valueOf(cursor.getLong(cursor.getColumnIndexOrThrow("album_id")));
            Uri albumArtUri = ContentUris.withAppendedId(Uri.parse("content://media/external/audio/albumart"), albumId.longValue());
            try {
                Picasso.get().load(albumArtUri).placeholder(R.drawable.speaker).into(imgThumbNail);
            } catch (Exception e) {
            }
        }
    }
}
