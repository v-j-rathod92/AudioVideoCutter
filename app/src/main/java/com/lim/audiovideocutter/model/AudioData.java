package com.lim.audiovideocutter.model;

public class AudioData {
    public boolean AChecked = false;
    public String ADuration;
    public String ASize;
    public int Aposition;
    public String Audioname;
    public String Audiopath;
    public Long album_id;

    public AudioData(String audioName, String audioPath, String Duration, String Size, int position, boolean ischecked, Long album_id) {
        this.Audioname = audioName;
        this.Audiopath = audioPath;
        this.ADuration = Duration;
        this.ASize = Size;
        this.Aposition = position;
        this.AChecked = ischecked;
        this.album_id = album_id;
    }

    public String getAudioName() {
        return this.Audioname;
    }

    public String getAudioPath() {
        return this.Audiopath;
    }

    public String getDuration() {
        return this.ADuration;
    }

    public String getSize() {
        return this.ASize;
    }

    public int getPosition() {
        return this.Aposition;
    }

    public boolean isSelected() {
        return this.AChecked;
    }

    public void setSelected(boolean selected) {
        this.AChecked = selected;
    }

    public Long getAlbumid() {
        return this.album_id;
    }
}
