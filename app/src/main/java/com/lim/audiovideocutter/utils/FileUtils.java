package com.lim.audiovideocutter.utils;

import android.os.Environment;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.List;

public class FileUtils {
    public static String getTargetFileName(String inputFileName) {
        final String fileName = new File(inputFileName).getAbsoluteFile().getName();
        int count = 0;
        List<String> fileList = Arrays.asList(new File(Environment.getExternalStorageDirectory() + "/VideoCutter").getAbsoluteFile().list(new FilenameFilter() {
            public boolean accept(File dir, String filename) {
                return filename != null && filename.startsWith("cut-") && filename.endsWith(fileName);
            }
        }));
        while (true) {
            StringBuilder append = new StringBuilder().append("cut-");
            Object[] objArr = new Object[1];
            int count2 = count + 1;
            objArr[0] = Integer.valueOf(count);
            String targetFileName = append.append(String.format("%03d", objArr)).append("-").append(fileName).toString();
            if (!fileList.contains(targetFileName)) {
                return new File(Environment.getExternalStorageDirectory() + "/VideoCutter", targetFileName).getPath();
            }
            count = count2;
        }
    }
}