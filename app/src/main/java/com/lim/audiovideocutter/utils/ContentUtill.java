package com.lim.audiovideocutter.utils;

import android.database.Cursor;

public class ContentUtill {
    public static String getLong(Cursor cursor) {
        return "" + cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
    }

    public static int getInt(Cursor cursor, String columnName) {
        return cursor.getInt(cursor.getColumnIndexOrThrow(columnName));
    }
}
